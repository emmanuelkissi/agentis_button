from odoo import fields, models


class AccountMove(models.Model):
    _inherit = "account.move"

    def button_set(self):
        company_ids = self.env['res.company'].search([])
        print("Button set exec")
        for company_id in company_ids:
            company_id.chart_template_id = 1
